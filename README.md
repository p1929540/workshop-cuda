# Workshop-Cuda

NB : All the images (or almost all) are taken from https://docs.nvidia.com/cuda/cuda-c-programming-guide/

If any problem on Linux : maybe see here https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html

For windows : tips of GPT (didn't tested it...) https://chatgpt.com/c/6fc30d9b-de1c-42c4-8b9d-0fcb5a5c4a59

## Getting started

To declare a function that is executes on the GPU : \_\_global\_\_ void kernel(Parameters : mainly pointers).

To allocate a buffer on the GPU : cudaMalloc (works the same as the C malloc)

To copy data : cudaMemcpy(dst, src, size, whereTo).

NB : for cudaMalloc and cudaMemcpy, the size is in bytes.
NB : whereTo is a enum, the 2 main values are cudaMemcpyHostToDevice and cudaMemcpyDeviceToHost.
NB : Host specify everything that is not the device (= GPU)

## Calling a kernel

### Threads orgonization

Threads are abstractly organized in blocks, organized in grids.

![](img/threadOrganisation.png)

Here, an arrow represents a thread. This organization is important for memory management, but does not concern us here.
When you call a kernel from the host, you have to specify the number of block in a grid, and the number of threads per blocks.

ex : <<<nbGrid, nbBlock>>>.

NB : You can specify the dispatchment in multiple dimensions : <<<{10,1,1}, {1,1,1}>>>.

In a kernel, you can use some constant to retrieve the id of your threads, namely threadIdx and blockIdx.

NB : Except in certain cases, you can't launch the exact number of threads you want. You must call the kernel with more threads
than actually needed. You must test if the id of the threads respects the condition.

NB : The way you dispatch the threads and the data they will be treating is absolutly arbitrary. However, do keep in mind that you want threads that are close in the dispatchment to treat data that are close in memory. Then again, this does not concern us.

![](img/memory-hierarchy.png)

## The workshop

This workshop won't be a cuda tutorial, because other ressources on the internet will be better for that.
Instead, we will be talking about the more hardwared side of a gpu.

### Check of the things I want to talk about (this part is more for me)

[] Synchronization CPU <-> GPU
[] SIMT architecture
[] Warp functions
[] Tensor core
