all: mainSync mainSync wmma warpUtility warpUse
BIN= bin
SRC= src

ARCH= sm_80

%: $(SRC)/%.cu
	nvcc -g -arch=$(ARCH) $< -o $(BIN)/$@

%: $(SRC)/%.cpp
	g++ -std=c++20 $< -o $(BIN)/$@

clean:
	rm -f $(BIN)/*