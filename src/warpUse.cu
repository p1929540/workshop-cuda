#include <cassert>
#include <cuda.h>
#include <iostream>
#include <mma.h>





int main(int argc, const char** argv) {

    std::cout <<"Dans le programme : \n";
    unsigned char *A, *B, *a, *b;
    int *resGPU, *res;
    // const int nbWarpPerBlock = 1024/32;

    if (argc != 4) {
        std::cerr << "Please enter 4 args\n";
        exit(EXIT_FAILURE);
    }

    int m = std::stoi(argv[1]),
        n = std::stoi(argv[2]),
        k = std::stoi(argv[3]);

    assert(m%16 == 0 && n%16 == 0 && k%16 == 0);

    a = new unsigned char[m*k];
    b = new unsigned char[k*n];
    res = new int[m*n];

    for(int i = 0; i < m; ++i) {
        for (int j = 0; j < k; ++j) {
            a[i*k + j] = static_cast<unsigned char>(j);
        }
    }

    for(int i = 0; i < m; i++)  {
        for (int j = 0; j < k; j++) {
            std::cout << static_cast<int>(a[i*n + j]) << ",";
        }
        std::cout << "\n";
    }

    std::cout << std::endl;
    for(int i = 0; i < n; ++i) {
        for (int j = 0; j < k; ++j) {
            b[i*k + j] = static_cast<unsigned char>(j);
        }
    }

    for(int i = 0; i < n; i++)  {
        for (int j = 0; j < k; j++) {
            std::cout << static_cast<int>(b[i*k + j]) << ",";
        }
        std::cout << "\n";
    }

    std::cout << std::endl;

    int nbWarp = m*n/(16*16);
    int nbBlock = m*n/1024;
    std::cout << "Number of block : " << nbBlock
    << "\nNumber of warp needeed : " << nbWarp << std::endl;

    cudaMalloc(&A, sizeof(unsigned char)*m*k);
    cudaMalloc(&B, sizeof(unsigned char)*k*n);
    cudaMalloc(&resGPU, sizeof(int)*m*n);

    cudaMemcpy(A, a, sizeof(unsigned char)*m*k, cudaMemcpyHostToDevice);
    cudaMemcpy(B, b, sizeof(unsigned char)*k*n, cudaMemcpyHostToDevice);
    
    if(nbBlock > 0 )
        kernel_wmma_mm<unsigned char, int,16,16,16><<<nbBlock,1024>>>(resGPU, A, B, m, n, k);
    else
        kernel_wmma_mm<unsigned char, int,16,16,16><<<1,32>>>(resGPU, A, B, m, n, k);

    cudaError_t error = cudaGetLastError();
    if (error != cudaSuccess) {
        std::cout << cudaGetErrorString(error) << std::endl;
        exit(EXIT_FAILURE);
    }

    cudaMemcpy(res, resGPU, sizeof(int)*m*n, cudaMemcpyDeviceToHost);

    for(int i = 0; i < m; i++)  {
        for (int j = 0; j < n; j++) {
            std::cout << res[i*n + j] << ",";
        }
        std::cout << "\n";
    }

    cudaFree(A);
    cudaFree(B);
    cudaFree(res);
    delete [] a;
    delete [] b;
    return 0;
}