#include <cassert>
#include <cuda.h>
#include <iostream>


__global__ void kernel_wmma_mm(int *res, unsigned char *A, unsigned char *b) {

}

int main(int argc, const char** argv) {

    std::cout <<"Dans le programme : ";
    unsigned char *A, *B, *a, *b;
    int *resGPU, *res;
    const int nbWarpPerBlock = 1024/32;

    if (argc != 4) {
        std::cerr << "Please enter 4 args\n";
        exit(EXIT_FAILURE);
    }

    int m = std::stoi(argv[1]),
        n = std::stoi(argv[2]),
        k = std::stoi(argv[3]);

    assert(m%16 == 0 && n%16 == 0 && k%16 == 0);

    a = new unsigned char[m*k];
    b = new unsigned char[k*n];
    res = new int[m*n];

    for(int i = 0; i < m*k; ++i) {
        a[i] = static_cast<unsigned char>(1);
    }

    for(int i = 0; i < k*n; ++i) {
        b[i] = static_cast<unsigned char>(1);
    }

    int nbWarp = m*n/(16*16);

    std::cout << "Number of warp needeed : " << nbWarp << std::endl;

    cudaMalloc(&A, sizeof(unsigned char)*m*k);
    cudaMalloc(&B, sizeof(unsigned char)*k*n);
    cudaMalloc(&resGPU, sizeof(int)*m*n);

    cudaMemcpy(A, a, sizeof(unsigned char)*m*k, cudaMemcpyHostToDevice);
    cudaMemcpy(B, b, sizeof(unsigned char)*k*n, cudaMemcpyHostToDevice);
    
    kernel_wmma_mm<<<1,m*n>>>(resGPU, A, B);
    cudaError_t error = cudaGetLastError();
    if (error != cudaSuccess) {
        std::cout << cudaGetErrorString(error) << std::endl;
        exit(EXIT_FAILURE);
    }

    cudaMemcpy(res, resGPU, sizeof(int)*m*n, cudaMemcpyDeviceToHost);

    for(int i = 0; i < m; i++)  {
        for (int j = 0; j < n; j++) {
            std::cout << res[i*n + j] << ",";
        }
        std::cout << "\n";
    }

    cudaFree(A);
    cudaFree(B);
    cudaFree(res);
    delete [] a;
    delete [] b;
    return 0;
}