#include <iostream>
#include <thread>
#include <vector>
#include <latch>

std::latch l1(8), l2(8);
void job(unsigned int idThread, int& res) {

    l1.arrive_and_wait();
    if (idThread == 0) res += idThread;
    if (idThread == 1) res += idThread;
    if (idThread == 2) res += idThread;
    if (idThread == 3) res += idThread;
    if (idThread == 4) res += idThread;
    if (idThread == 5) res += idThread;
    if (idThread == 6) res += idThread;
    if (idThread == 7) res += idThread;
    l2.arrive_and_wait();
}

int main() {

    std::vector<std::thread> tabThread;
    int res = 0;
    for(int i = 0; i < 8; ++i) tabThread.push_back(std::thread(job, i, std::ref(res)));

    for(auto& t : tabThread) t.join();

    std::cout << res << std::endl;
    return 0;
}