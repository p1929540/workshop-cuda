#include <cuda.h>
#include <iostream>


__global__ void kernel(int* A) {
    int idThread = threadIdx.x;

    __syncwarp();
    if (idThread == 0) *A += idThread;
    if (idThread == 1) *A += idThread;
    if (idThread == 2) *A += idThread;
    if (idThread == 3) *A += idThread;
    if (idThread == 4) *A += idThread;
    if (idThread == 5) *A += idThread;
    if (idThread == 6) *A += idThread;
    if (idThread == 7) *A += idThread;
    if (idThread == 8) *A += idThread;
    if (idThread == 9) *A += idThread;
    if (idThread == 10) *A += idThread;
    if (idThread == 11) *A += idThread;
    if (idThread == 12) *A += idThread;
    if (idThread == 13) *A += idThread;
    if (idThread == 14) *A += idThread;
    if (idThread == 15) *A += idThread;
    if (idThread == 16) *A += idThread;
    if (idThread == 17) *A += idThread;
    if (idThread == 18) *A += idThread;
    if (idThread == 19) *A += idThread;
    if (idThread == 20) *A += idThread;
    if (idThread == 21) *A += idThread;
    if (idThread == 22) *A += idThread;
    if (idThread == 23) *A += idThread;
    if (idThread == 24) *A += idThread;
    if (idThread == 25) *A += idThread;
    if (idThread == 26) *A += idThread;
    if (idThread == 27) *A += idThread;
    if (idThread == 28) *A += idThread;
    if (idThread == 29) *A += idThread;
    if (idThread == 30) *A += idThread;
    if (idThread == 31) *A += idThread;
    __syncwarp();

} 


int main() {

    std::cout <<"Dans le programme : ";
    int* A;
    int i = 0;

    

    cudaMalloc(&A, sizeof(int));
    cudaMemcpy(A, &i, sizeof(int), cudaMemcpyHostToDevice);
    kernel<<<1,32>>>(A);
    cudaError_t error = cudaGetLastError();
    if (error != cudaSuccess) {
        std::cout << cudaGetErrorString(error) << std::endl;
    }
    cudaMemcpy(&i, A, sizeof(int), cudaMemcpyDeviceToHost);

    std::cout << i << std::endl;

    cudaFree(A);
    return 0;
}