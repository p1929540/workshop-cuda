#include <stdio.h>
#include <cuda.h>
#include <mma.h>



using namespace nvcuda;

template<typename Tmult, typename Tacc, unsigned int nbRow, unsigned int nbCol, unsigned int nbCommonDim>
__global__ void kernel_wmma_mm_16x16(Tacc *r, Tmult *a, Tmult *b) {
   // Declare the fragments
   wmma::fragment<wmma::matrix_a, nbRow, nbCol, nbCommonDim, Tmult, wmma::row_major> a_frag;
   wmma::fragment<wmma::matrix_b, nbRow, nbCol, nbCommonDim, Tmult, wmma::col_major> b_frag;
   wmma::fragment<wmma::accumulator, nbRow, nbCol, nbCommonDim, Tacc> r_frag;

   // Load the inputs
   wmma::load_matrix_sync(r_frag, r, nbRow, wmma::mem_col_major);
   wmma::load_matrix_sync(a_frag, a, nbCommonDim);
   wmma::load_matrix_sync(b_frag, b, nbCommonDim);

   // Perform the matrix multiplication
   wmma::mma_sync(r_frag, a_frag, b_frag, r_frag);

   // Store the output
   wmma::store_matrix_sync(r, r_frag, nbCol, wmma::mem_row_major);
}

template<typename Tmult, typename Tacc, unsigned int nbRow, unsigned int nbCol, unsigned int nbCommonDim>
void wmma_mm_16x16(Tacc *Rh, Tmult *Ah, Tmult *Bh) {
   Tmult *Ad, *Bd;
   Tacc *Rd;

   cudaError_t err = cudaMalloc((void**)&Ad, nbRow*nbCommonDim * sizeof(Tmult));
   if(err != cudaSuccess) {
      printf("%s\n", cudaGetErrorString(err));
   }
   
   err = cudaMalloc((void**)&Bd, nbCommonDim*nbCol * sizeof(Tmult));
   if(err != cudaSuccess) {
      printf("%s\n", cudaGetErrorString(err));
   }

   err = cudaMalloc((void**)&Rd, nbRow*nbCol * sizeof(Tacc));
   if(err != cudaSuccess) {
      printf("%s\n", cudaGetErrorString(err));
   }

   err = cudaMemcpy(Ad, Ah, nbRow*nbCommonDim * sizeof(Tmult), cudaMemcpyHostToDevice);
   if(err != cudaSuccess) {
      printf("%s\n", cudaGetErrorString(err));
   }

   err = cudaMemcpy(Bd, Bh, nbCommonDim*nbCol * sizeof(Tmult), cudaMemcpyDeviceToHost);
   if(err != cudaSuccess) {
      printf("%s\n", cudaGetErrorString(err));
   }
   
   err = cudaMemcpy(Rd, Rh, nbRow*nbCol * sizeof(Tacc), cudaMemcpyHostToDevice);
   if(err != cudaSuccess) {
      printf("%s\n", cudaGetErrorString(err));
   }

   kernel_wmma_mm_16x16<Tmult, Tacc, nbRow, nbCol, nbCommonDim> <<< 1,32 >>> (Rd, Ad, Bd);
   err = cudaGetLastError();
   if(err != cudaSuccess) {
      printf("%s\n", cudaGetErrorString(err));
   }

   err = cudaMemcpy(Rh, Rd, nbRow*nbCol * sizeof(Tacc), cudaMemcpyDeviceToHost);
   if(err != cudaSuccess) {
      printf("%s\n", cudaGetErrorString(err));
   }
   
   cudaFree(Ad);
   cudaFree(Bd);
   cudaFree(Rd);
}

float fp2float(nv_bfloat16 x)  { return __bfloat162float(x); }
float fp2float(float x) { return x; }

void fpset(nv_bfloat16 &d, float s)  { d = nv_bfloat16(s); }
void fpset(float &d, float s) { d = s; }

template<typename Tmult, typename Tacc, unsigned int nbRow, unsigned int nbCol, unsigned int nbCommonDim>
void test_wmma_mm_16x16(void) {
   Tacc *C;
   Tmult *A, *B;

   A = (Tmult*)malloc(nbRow*nbCommonDim * sizeof(Tmult));
   B = (Tmult*)malloc(nbCommonDim*nbCol * sizeof(Tmult));
   C = (Tacc*)malloc(nbRow*nbCol * sizeof(Tacc));

   for(int i=0; i < nbRow; i++) {
      for(int j = 0; j < nbCommonDim; j++){
         A[i*nbCommonDim + j] = nv_bfloat16(i);
      }
   }

   for(int i=0; i<nbRow; i++) {
     printf("%i : ", i);
     for(int j=0; j<nbCommonDim; j++) printf("%f ", fp2float(A[i*nbCommonDim + j]));
     printf("\n");
   }
   
   for(int i=0; i < nbCol; i++) {
      for (int j = 0; j < nbCommonDim; ++j){
         B[i*nbCommonDim + j] = nv_bfloat16(1.0);
      }
   }

   printf("<----------------------------------------------->\n");
   for(int i=0; i<nbCol; i++) {
     printf("%i : ", i);
     for(int j=0; j<nbCommonDim; j++) printf("%f ", fp2float(B[i*nbCommonDim + j]));
     printf("\n");
   }

   for(int i=0; i < nbRow*nbCol; i++) {
      C[i] = nv_bfloat16(0.0);
   }

   wmma_mm_16x16<Tmult, Tacc, nbRow, nbCol, nbCommonDim>(C, A, B);

   cudaDeviceSynchronize();

   printf("<----------------------------------------------->\n");
   for(int i=0; i<nbRow; i++) {
      printf("%i : ", i);
     for(int j=0; j<nbCol; j++) printf("%f ", fp2float(C[i*nbCol + j]));
     printf("\n");
   }

   free(A);
   free(B);
   free(C);
}

int main(void) {
   printf("FB16 (float) mode:\n");
   test_wmma_mm_16x16<nv_bfloat16, float, 16, 16, 16>(); 

   /*printf("FP16 (nv_bfloat16) mode:\n");
   test_wmma_mm_16x16<nv_bfloat16>();*/
   
   return 0;   
}
